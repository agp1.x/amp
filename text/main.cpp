#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "txtacc.h"
//extern "C" char *  gets (const char * str);
#define  BUFSZ 1000
//     ���������� �� ����������� �����������
void usage(const char * nm){
  char appNm[100] = {0};
  char buf[200] = {0};
  strcpy(buf, nm);
  // to separate name of file and path to him
  char *tmp = strrchr (buf, '\\');// windows style c:\tmp\a.exe
  if (tmp)
      strcpy(appNm, tmp+1);
  else {
      tmp = strrchr (buf, '/');   // unix style /cygdrive/tmp/a.exe
      if (tmp)
         strcpy(appNm, tmp+1);
      else 
         strcpy(appNm,nm);
  }
  printf("application to test the text accumulator\n");
  printf("usage:\n");
  printf("%s  [-?] [-v]\nwhere\n", appNm);
  printf("-?	this page\n");
  printf("-v	to show additional info\n");
  exit(1);  // ��� ���������� ������ ���������� �� 0
}

int vFlag  = 0 ; // global veriable to print additional info




int main(int argc, char *argv[]) {
  char buf[BUFSZ] = {0};
  int lineno = 0;
  int i = 1;

 //    to check the command line here
  for (  ; i< argc; i++){
     if(strcmp(argv[i], "-?") == 0 )
        usage (argv[0]);
     else if(strcmp(argv[i], "-h") == 0 )
        usage (argv[0]);
#ifndef  _MSC_VER
     else if (strcasecmp(argv[i], "-v") == 0 )
#else 
     else if (stricmp(argv[i], "-v") == 0 )
#endif
        vFlag = 1;
  }


 //    to work here
  while (gets(buf)){
    lineno++;
    addStr(buf);
  }
  if (errno) {  // ��������� ������ ��� ����� �����
        perror ("error while reading stdin");
        fprintf(stderr, "line No: %d; error: %d/'%s'"
            , lineno, errno, strerror(errno));
        exit (1);
  }
  if (vFlag)
      fprintf (stderr, "\nmain: I have red %d lines"
                                , lineno);
  priMem();
  freeMem();
  return 0;
}
//  __CYGWIN32__  _MSC_VER  _WIN32
