#include <stdio.h>
#include <string.h>
// pragma is used here to avoid message about the macro redefenition
#pragma warning(disable : 4005)  
#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1

int main() {       
   char  buf[1000] = {0};
   gets(buf);
   printf ("\n your input: '%s'", buf);
   // to test  _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES feature
   // cl.exe gets_s.cpp  /o a.exe /E
   strcpy(buf, "test");   
}
 
//  I am't able to check gcc.exe option
//   g++ -std=c++11 gets_s.cpp  
//   and see what is wrong.
//   My gcc works in any case
//https://docs.microsoft.com/en-us/cpp/build/reference\
//           /std-specify-language-standard-version?view=msvc-160
//
//    /std:c++14
//    /std:c++17
//    /std:c++latest
//    /std:c11
//    /std:c17
