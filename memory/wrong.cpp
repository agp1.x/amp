#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
int c = 30;
int main() {   
    register int  a = 10;
    int           b = 20;
    int * pa = &a;        // error is here
    printf ("\nregister  %u, %d", pa, *pa);
    printf(  "\nerror: %d: %s", errno,   strerror(errno) );
    pa = &b;
    printf ("\nautomatic %u, %d", pa, *pa);
    pa = &c;
    printf ("\nstatic  %u, %d", pa, *pa);
    pa = (int*)malloc(sizeof(int));
    printf ("\ndinamic %u, %d", pa, *pa);
}