#include <stdio.h>
#include <stdlib.h>
typedef  unsigned int uint;

void print(const char * name,  const int **a, uint lines, uint columns){
  if (name && a) {
    printf ("\n  '%s'", name);
    for ( int l = 0; l < lines; l++) {
      printf ("\nline No: %d: ", l);
      for (int c = 0; c < columns; c++)
        printf (" %d ", a[l][c]);
    }
  }
  else 
    fprintf (stderr,"\n nothing to do");
}

int main() {
  int **p = 0;
  p    = (int **)  malloc( sizeof(int*) *2);
      // использование sizeof  желательно до обязательсности

  p[0] = (int *)   malloc(sizeof(int) *3);
    p[0][0] = 1;
    p[0][1] = 20;
    p[0][2] = 30;

  p[1] = (int *)   malloc(sizeof(int) *3);
    p[1][0] = 2;
    p[1][1] = 40;
    p[1][2] = 50;

  print("a array", (const int**)p, 2, 3);
  free(p[0]); p[0] = 0;   //  освобождение памяти 
  free(p[1]); p[1] = 0;   //  выполняется в порядке обратном 
  free(p);    p    = 0;   //  захвату
}
