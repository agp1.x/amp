#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <errno.h>
int main() {
   int *pi = new int();
   delete pi; pi = 0;
   printf(  "pi/errno/message: %u/%d/%s",  pi, errno, strerror(errno) );
   delete pi;
   printf(  "\npi/errno/message: %u/%d/%s",  pi, errno, strerror(errno) );
   int *array = new int[10];
   array[9] = 999;
   printf(  "\narray[4]/array[9]: %d/%d",  array[4], array[9] );
   delete []array; array = 0;
}

              