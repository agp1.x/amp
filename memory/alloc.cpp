#include <stdio.h>
#include <malloc.h>
#include <limits.h>
#include <string.h>
#include <errno.h>
int main() {
    int * pa = 0;
    pa = (int *) malloc  (10);
    pa = (int *) realloc (pa, 20);
    if (pa) {
      free (pa);
      pa = 0;
    }
    pa = (int *) malloc(UINT_MAX);  
    printf(  "pa/errno/message: %u/%d/%s",  pa, errno, strerror(errno) );
    if (pa) {
    	free (pa); 
    	pa = 0; 
    }
    free(pa); // error is here
    printf(  "\nfree(0): pa/errno/message: %u/%d/%s",  pa, errno, strerror(errno) );
}              