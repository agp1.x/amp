#include <stdio.h>
#include <limits.h>
#include <float.h>

int main() {     
   printf (" min/max of integer: %d / %d" , INT_MIN, INT_MAX);
   printf ("\n -min/+min/+max of double: %e / %e / %e" , -DBL_MAX, DBL_MIN, DBL_MAX);
}
