#include <iostream>
using namespace std;  

short fact(short n) {
     if (n > 0)
       return n* fact(n-1);
     else 
       return 1;
}

int main() {
  short    n;
  cin>>n;
  if (n >= 0)
    cout<< " number:" << n  << " factorial: "<<fact(n) << "\n";
  else
    cout<< " number:" << n  << " no any factorial\n";
}                 