#include <iostream>
using namespace std;  

inline short fact(short n) { return (n > 0)? n*fact(n-1) : 1; }

int main() {
  short    n;
  cin>>n;
  if (n >= 0)
    cout<< " number:" << n  << " factorial: "<<fact(n) << "\n";
  else
    cout<< " number:" << n  << " no any factorial\n";
}                 