#include <stdio.h>
int main() {     
  double a[3];
  double *pa= &a[1];
  int    addr = (int) &a[1];
  printf ("pa/addr: %d/%d\n", pa++, addr++) ;
  printf ("pa/addr: %d/%d/%d\n", pa, addr, ((int)pa)-addr) ;
}