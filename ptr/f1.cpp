#include <stdio.h>

double plus (int a, int b)     { return ((double)a + b);}
double minus (int a, int b)    { return ((double)a - b);}
double divide (int a, int b)   { return ((double)a / b);}
double multiply (int a, int b) { return ((double)a * b);}
typedef double ( *fun )( int a, int b ) ;  // new line

int main() {
   fun f = 0;           // modifyed line
   f = plus;
   fun arr[] =          // modifyed line
          {plus, minus, divide, multiply};
  char * names [] = {
     "plus", "minus", "divide", "multiply"
  };
  int a = 3, b = 2;

  printf ("%d %s %d = %f \n", 
      a,  "plus", b, f(a,b));

  for (int i = 0; i < 4; i++) 
    printf ("%d %s %d = %.1f \n", 
      a,  names[i], b, arr[i](a,b));
}
