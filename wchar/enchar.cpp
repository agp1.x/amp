#include <stdio.h>
int main() {     
  wchar_t  y =  L'z';  
  char     x =  'z';   
  putchar(y);
  putwchar(y);
  putchar(x);
  putwchar(x);

  printf ("\nit is %S: x:%c/%C y:%c/%C<<\n", L"printf", x,x,  y, y ) ;   // big   %S
  wprintf (L"it is %s: x:%c/%C y:%c/%C<<\n", L"wprintf", x,x,  y, y  ) ; // small %s
  printf ("\ncode: %d/%d\n", x,  y) ;
  printf ("sizeof: %d/%d\n", sizeof(x),  sizeof(y)) ;
}