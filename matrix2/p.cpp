#include <stdio.h>
#include <stdlib.h>
#define UINT   unsigned int
void print(const char * name,  const int **a, UINT lines, UINT columns) {
  if (name && a) {
    printf ("\n  '%s'", name);
    for ( int l = 0; l < lines; l++) {
      printf ("\nline No: %d: ", l);
      for (int c = 0; c < columns; c++)
        printf (" %d ", a[l][c]);
    }
  }
  else 
    fprintf (stderr,"\n nothing to do");
}
int main() {
  int *p[10] = {0};
  int a [2] [3] =    { {1, 20, 30}, {2, 40, 50} };
  p [0] = a [0];
  p [1] = a [1];
  print("a array", (const int**)p, 2, 3);
  int b [3] [4] =
    {{1, 200, 300, 400}, {2, 500, 600, 700}, {3, 800, 900, 100}};
  p [0] = b [0];
  p [1] = b [1];
  p [2] = b [2];
  print("b array", (const int**)p, 3, 4);
}
