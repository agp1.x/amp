#include <stdlib.h>
#include <stdio.h>
#include <crtdbg.h>
//����� ��������������� ��� ���������������� ������ 
// � ���������� �� ���������� ������ ����������
int 	main() {
  _CrtSetDbgFlag(
   _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG)
                  | _CRTDBG_CHECK_ALWAYS_DF 
                  | _CRTDBG_LEAK_CHECK_DF
      );

  _CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE);
  _CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDERR);
  _CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE);
  _CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDERR);
  _CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_FILE);
  _CrtSetReportFile(_CRT_ERROR, _CRTDBG_FILE_STDERR);
   
   char *s = (char*) malloc(5);  //free(s);  error #1
   s[5] = 'z';                   // it's  some error #2, last one has index 4
   char * p = new char('d');     // delete p; p = 0;  error #3
   printf("hi, world");
   return 0;
}
