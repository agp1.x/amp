#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#define MEMWATCH
#define MW_STDIO

#include "memwatch.h"

int main(){
    /* Collect stats on a line number basis */
    mwStatistics( 2 );
//    TRACE("Hello world!\n");

    mwTrace( "Hello world!\n" );
    char * s = malloc(10);

//    new int();     // to check new
    printf ("hello");
    //CHECK();

   return 0;
}
