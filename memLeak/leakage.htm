<!--
<!DOCTYPE html SYSTEM "http://wyw.dcweb.cn/loose401.dtd">

Note: I did not use the above DOCTYPE since it will trigger the full
      standards mode in Mozilla, which renders my page uglier than the
      default quirks mode.  I don't know what to say about it....

-->
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <meta name="Author" content="Wu Yongwei">
  <meta name="Keywords" content="debug_new, memory leakage, leak detector">
  <title>A Cross-Platform Memory Leak Detector</title>
  <link rel="stylesheet" href="normal.css">
  <style type="text/css">
  <!--
  .code { border: 1px solid; border-color: green; padding: 4pt }
  .filename { font: smaller Verdana,Arial,Helvetica,sans-serif }
  -->
  </style>
</head>
<body>

<h1>A Cross-Platform Memory Leak Detector<img src="counter.asp?name=leakage" width="0" height="0"></h1>
<p>Memory leakage has been a permanent annoyance for C/C++ programmers.
Under MSVC, one useful feature of MFC is report memory leaks at the exit
of an application (to the debugger output window, which can be displayed
by the integration environment or a debugger). Under GCC, current
available tools like mpatrol are relatively difficult to use, or have a
big impact on memory/performance. This article details the
implementation of an easy-to-use, cross-platform C++ memory leak
detector (which I call <em>debug_new</em>), and discusses the related
technical issues.
</p>

<h2>Basic usage</h2>
<p>Let&rsquo;s look at the following simple program <span
class="filename">test.cpp</span>: </p>
<blockquote>
<pre class="code">
<font color="#2e8b57"><b>int</b></font> main()
{
    <font color="#2e8b57"><b>int</b></font>* p1 = <font color="#804040"><b>new</b></font> <font color="#2e8b57"><b>int</b></font>;
    <font color="#2e8b57"><b>char</b></font>* p2 = <font color="#804040"><b>new</b></font> <font color="#2e8b57"><b>char</b></font>[<font color="#ff00ff">10</font>];
    <font color="#804040"><b>return</b></font> <font color="#ff00ff">0</font>;
}
</pre>
</blockquote>
<p>Our basic objectives are, of course, report two memory leaks. It is
very simple: just compile and link <span class="filename">debug_new.cpp</span>.
For example:
</p>
<blockquote>
  <table width="85%" cellpadding="0" cellspacing="0" border="0">
    <tbody>
      <tr>
        <td><code>cl -GX test.cpp debug_new.cpp</code></td>
        <td>(MSVC)</td>
      </tr>
      <tr>
        <td><code>g++ test.cpp debug_new.cpp -o test</code></td>
        <td>(GCC)</td>
      </tr>
    </tbody>
  </table>
</blockquote>
<p>The running output is like follows:
</p>
<blockquote>
<pre>
Leaked object at 00341008 (size 4, &lt;Unknown&gt;)
Leaked object at 00341CA0 (size 10, &lt;Unknown&gt;)
</pre>
</blockquote>
<p>If we need clearer reports, it is also trivial: just put this at the
front of <span class="filename">test.cpp</span>:
</p>
<blockquote>
<pre class="code">
<font color="#a020f0">#include </font><font color="#ff00ff">"debug_new.h"</font>
</pre>
</blockquote>
<p>The output after adding this line is:
</p>
<blockquote>
<pre>
Leaked object at 00340FB8 (size 10, test5.cpp:5)
Leaked object at 00340F80 (size 4, test5.cpp:4)
</pre>
</blockquote>
<p>Very simple, isn&rsquo;t it?
</p>

<h2>Background knowledge</h2>
<p>In a <strong>new</strong>/<strong>delete</strong> operation, C++
compilers generates calls to <strong>operator new</strong> and
<strong>operator delete</strong> (allocation and deallocation functions)
for the user. The prototypes of <strong>operator new</strong> and
<strong>operator delete</strong> are as follows:
</p>
<blockquote>
<pre class="code">
<font color="#2e8b57"><b>void</b></font>* <font color="#804040"><b>operator</b></font> <font color="#804040"><b>new</b></font>(<font color="#2e8b57"><b>size_t</b></font>) <font color="#804040"><b>throw</b></font>(std::bad_alloc);
<font color="#2e8b57"><b>void</b></font>* <font color="#804040"><b>operator</b></font> <font color="#804040"><b>new</b></font>[](<font color="#2e8b57"><b>size_t</b></font>) <font color="#804040"><b>throw</b></font>(std::bad_alloc);
<font color="#2e8b57"><b>void</b></font> <font color="#804040"><b>operator</b></font> <font color="#804040"><b>delete</b></font>(<font color="#2e8b57"><b>void</b></font>*) <font color="#804040"><b>throw</b></font>();
<font color="#2e8b57"><b>void</b></font> <font color="#804040"><b>operator</b></font> <font color="#804040"><b>delete</b></font>[](<font color="#2e8b57"><b>void</b></font>*) <font color="#804040"><b>throw</b></font>();
</pre>
</blockquote>
<p>For <strong>new int</strong>, the compiler will generate a call to
&ldquo;<code>operator new(sizeof(int))</code>&rdquo;, and for
<strong>new char[10]</strong>, &ldquo;<code>operator new(sizeof(char) *
10)</code>&rdquo;. Similarly, for <strong>delete <i>ptr</i></strong>
and <strong>delete[] <i>ptr</i></strong>, the compiler will generate
calls to &ldquo;<code>operator delete(<i>ptr</i>)</code>&rdquo; and
&ldquo;<code>operator delete[](<i>ptr</i>)</code>&rdquo;. When the user
does not define these operators, the compiler will provide their
definitions automatically; when the user do define them, they will
override the ones the compiler provides. And we thus get the ability to
trace and control dynamic memory allocation.
</p>
<p>In the meanwhile, we can adjust the behaviour of <strong>new</strong>
operators with <strong>new</strong>-placements, which are to supply
additional arguments to the allocation functions. E.g., when we have a
prototype
</p>
<blockquote>
<pre class="code">
<font color="#2e8b57"><b>void</b></font>* <font color="#804040"><b>operator</b></font> <font color="#804040"><b>new</b></font>(<font color="#2e8b57"><b>size_t</b></font> size, <font color="#2e8b57"><b>const</b></font> <font color="#2e8b57"><b>char</b></font>* file, <font color="#2e8b57"><b>int</b></font> line);
</pre>
</blockquote>
<p>we may use <strong>new ("hello", 123) int</strong> to generate a call
to &ldquo;<code>operator new(sizeof(int), "hello", 123)</code>&rdquo;.
This can be very flexible. One placement allocation function that the
C++ standard (<a href="#CPP1998">[C++1998]</a>) requires is
</p>
<blockquote>
<pre class="code">
<font color="#2e8b57"><b>void</b></font>* <font color="#804040"><b>operator</b></font> <font color="#804040"><b>new</b></font>(<font color="#2e8b57"><b>size_t</b></font> size, <font color="#2e8b57"><b>const</b></font> std::nothrow_t&amp;) <font color="#804040"><b>throw</b></font>();
</pre>
</blockquote>
<p>in which <code>nothrow_t</code> is usually an empty structure
(defined as &ldquo;<code>struct nothrow_t {};</code>&rdquo;), whose sole
purpose is to provide a type that the compiler can identify for overload
resolution. Users can call it via <strong>new (std::nothrow)
<i>type</i></strong> (<strong>nothrow</strong> is a constant of type
<strong>nothrow_t</strong>). The difference from the standard
<strong>new</strong> is that when memory allocation fails,
<strong>new</strong> will throw an exception, but
<strong>new(std::nothrow)</strong> will return a null pointer.
</p>
<p>One thing to notice is that there is not a corresponding syntax like
<strong>delete(std::nothrow) <i>ptr</i></strong>. However, a related
issue will be mentioned later in this article.
</p>
<p>For more information about the above-mentioned C++ language features,
please refer to <a href="#Stroustrup1997">[Stroustrup1997]</a>, esp.
sections 6.2.6, 10.4.11, 15.6, 19.4.5, and B.3.4. These features are key
to understanding the implementation described below.
</p>

<h2>Principle and basic implementation</h2>
<p>Similar to some other memory leakage detectors, <em>debug_new</em>
overrides <strong>operator new</strong>, and provides macros to do
substitues in user&rsquo;s programs. The relevant part in <span
class="filename">debug_new.h</span> is as follows:
</p>
<blockquote>
<pre class="code">
<font color="#2e8b57"><b>void</b></font>* <font color="#804040"><b>operator</b></font> <font color="#804040"><b>new</b></font>(<font color="#2e8b57"><b>size_t</b></font> size, <font color="#2e8b57"><b>const</b></font> <font color="#2e8b57"><b>char</b></font>* file, <font color="#2e8b57"><b>int</b></font> line);
<font color="#2e8b57"><b>void</b></font>* <font color="#804040"><b>operator</b></font> <font color="#804040"><b>new</b></font>[](<font color="#2e8b57"><b>size_t</b></font> size, <font color="#2e8b57"><b>const</b></font> <font color="#2e8b57"><b>char</b></font>* file, <font color="#2e8b57"><b>int</b></font> line);
<font color="#a020f0">#define DEBUG_NEW </font><font color="#804040"><b>new</b></font><font color="#a020f0">(</font><font color="#ff00ff">__FILE__</font><font color="#a020f0">, </font><font color="#ff00ff">__LINE__</font><font color="#a020f0">)</font>
<font color="#a020f0">#define </font><font color="#804040"><b>new</b></font><font color="#a020f0"> DEBUG_NEW</font>
</pre>
</blockquote>
<p>Let&rsquo;s look at the <span class="filename">test.cpp</span> after
including <span class="filename">debug_new.h</span>: <strong>new
char[10]</strong> will become &ldquo;<code>new("test.cpp", 4)
char[10]</code>&rdquo; after preprocessing, and the compiler will
generate a call to &ldquo;<code>operator new[](sizeof(char) * 10,
"test.cpp", 4)</code>&rdquo; accordingly. If I define
&ldquo;<code>operator new(size_t, const char*, int)</code>&rdquo; and
&ldquo;<code>operator delete(void*)</code>&rdquo; (as well as
&ldquo;<code>operator new[]...</code>&rdquo; and &ldquo;<code>operator
delete[]...</code>&rdquo;; for clarity, my discussions about
<strong>operator new</strong> and <strong>operator delete</strong> also
cover <strong>operator new[]</strong> and <strong>operator
delete[]</strong> without mentioning specifically, unless noted
otherwise) in <span class="filename">debug_new.cpp</span>, I can trace
all dynamic memory allocation/deallocation calls, and check for
unmatched <strong>new</strong>s and <strong>delete</strong>s. The
implementation may be as simple as using just a <code>map</code>: add a
pointer to <code>map</code> in <strong>new</strong>, and delete the
pointer and related information in <strong>delete</strong>; report wrong
deleting if the pointer to delete does not exist in the
<code>map</code>; report memory leaks if there are still pointers to
delete in the <code>map</code> at program exit.
</p>
<p>However, it will not work if <span class="filename">debug_new.h</span>
is not included. And the case that some translation units include <span
class="filename">debug_new.h</span> and some do not are unacceptable,
for although two <strong>operator new</strong>s are
used &mdash; &ldquo;<code>operator new(size_t, const char*,
int)</code>&rdquo; and &ldquo;<code>operator new(size_t)</code>&rdquo;
&mdash; there is only one <strong>operator delete</strong>! The
<strong>operator delete</strong> we define will consider it an invalid
pointer, when given a pointer returned by &ldquo;<code>operator
delete(void*)</code>&rdquo; (no information about it exists in the
<code>map</code>). We are facing a dilemma: either to misreport in this
case, or not to report when deleting a pointer twice: none is
satisfactory behaviour.
</p>
<p>So defining the global &ldquo;<code>operator new(size_t)</code>&rdquo; is
inevitable. In <span class="filename">debug_new.h</span>, I have
</p>
<blockquote>
<pre class="code">
<font color="#2e8b57"><b>void</b></font>* <font color="#804040"><b>operator</b></font> <font color="#804040"><b>new</b></font>(<font color="#2e8b57"><b>size_t</b></font> size)
{
    <font color="#804040"><b>return</b></font> <font color="#804040"><b>operator</b></font> <font color="#804040"><b>new</b></font>(size, <font color="#ff00ff">&quot;&lt;Unknown&gt;&quot;</font>, <font color="#ff00ff">0</font>);
}
</pre>
</blockquote>
<p>Implement the memory leak detector as I have described, you will find
it works under some environments (say, GCC 2.95.3 w/ SGI STL), but
crashes under others (MSVC 6 is among them). The reason is not
complicated: memory pools are used in SGI STL, and only large chunks of
memory will be allocated by <strong>operator new</strong>; in STL
implementations which do not utilize such mechanisms, adding data to
<code>map</code> will cause a call to <strong>operator new</strong>,
which will add data to <code>map</code>, and this dead loop will
immediately cause a stack overflow that aborts the application.
Therefore I have to stop using the convenient STL container and resort
to my own data structure:
</p>
<blockquote>
<pre class="code">
<font color="#2e8b57"><b>struct</b></font> new_ptr_list_t
{
    new_ptr_list_t*     next;
    <font color="#2e8b57"><b>const</b></font> <font color="#2e8b57"><b>char</b></font>*         file;
    <font color="#2e8b57"><b>int</b></font>                 line;
    <font color="#2e8b57"><b>size_t</b></font>              size;
};
</pre>
</blockquote>
<p>Every time one allocates memory via <strong>new</strong>, <code>sizeof(new_ptr_list_t)</code>
more bytes will be allocated when calling <strong>malloc</strong>. The
memory blocks will be chained together as a linked list (via the <code>next</code>
field), the file name, line number, and object size will be stored in
the <code>file</code>, <code>line</code>, and <code>size</code>
fields, and return <code>(</code><i>pointer-returned-by-malloc</i><code>
+ sizeof(new_ptr_list_t))</code>. When one <strong>delete</strong>s a
pointer, it will be matched with those in the linked list. If it does
match &mdash; <i>pointer-to-delete</i><code> == (char*)</code><i>pointer-in-linked-list</i><code>
+ sizeof(new_ptr_list_t)</code> &mdash; the linked list will be adjusted and
the memory deallocated. If no match is found, a message of deleting an
invalid pointer will be printed and the application will be <strong>abort</strong>ed.
</p>
<p>In order to automatically report memory leaks at program exit, I
construct a static object (C++ ensures that its constructor will be
called at program initialization, and the destructor be called at
program exit), whose destructor will call a function to check for
memory leaks. Users are also allowed to call this function manually.
</p>
<p>Thus is the basic implementation.
</p>

<h2>Improvements on usability</h2>
<p>The above method worked quite well, until I began to create a large
number of objects. Since each <strong>delete</strong> needed to search
in the linked list, and the average number of searches was a half of
the length of the linked list, the application soon <em>crawled</em>.
The speed was too slow even for the purpose of debugging. So I made a
modification: the head of the linked list is changed from a single
pointer to an array of pointers, and which element a pointer belongs to
depends on its hash value. &mdash; Users are allowed to change the definitions
of <code>_DEBUG_NEW_HASH</code> and <code>_DEBUG_NEW_HASHTABLESIZE</code>
(at compile-time) to adjust the behaviour of <em>debug_new</em>. Their
current values are what I feel satisfactory after some tests.
</p>
<p>I found in real use that under some special circumstances the
pointers to file names can become invalid (check the comment in <span
class="filename">debug_new.cpp</span> if you are interested).
Therefore, currently the default behaviour of <em>debug_new</em> is
copying the first 20 characters of the file name, instead of storing the
pointer to the file name. Also notice that the length of the original
<code>new_ptr_list_t</code> is 16 bytes, and the current length is 32
bytes: both can ensure correct memory alignments.
</p>
<p>In order to ensure <em>debug_new</em> can work with <strong>new(std::nothrow)</strong>,
I overloaded &ldquo;<code>void* operator new(size_t size, const
std::nothrow_t&amp;) throw()</code>&rdquo; too; otherwise the pointer
returned by a <strong>new(std::nothrow)</strong> will be considered an
invalid pointer to delete. Since <em>debug_new</em> does not throw
exceptions (the program will report an alert and abort when memory is
insufficient), this overload just calls <code>operator
new(size_t)</code>. Very simple.
</p>
<p>It has been mentioned previously that a C++ file should include <span
class="filename">debug_new.h</span> to get an accurate memory leak
report. I usually do this:
</p>
<blockquote>
<pre class="code">
<font color="#a020f0">#ifdef _DEBUG</font>
<font color="#a020f0">#include </font><font color="#ff00ff">&quot;debug_new.h&quot;</font>
<font color="#a020f0">#endif</font>
</pre>
</blockquote>
<p>The include position should be later than the system headers, but
earlier than user&rsquo;s own header files if possible. Typically <span
class="filename">debug_new.h</span> will conflict with STL header files
if included earlier. Under some circumstances one may not want
<em>debug_new</em> to redefine <strong>new</strong>; it could be done by
defining <code>_DEBUG_NEW_REDEFINE_NEW</code> to <code>0</code> before
including <span class="filename">debug_new.h</span>. Then the user
should also use <code>DEBUG_NEW</code> instead of <strong>new</strong>.
Maybe one should write this in the source:
</p>
<blockquote>
<pre class="code">
<font color="#a020f0">#ifdef _DEBUG</font>
<font color="#a020f0">#define _DEBUG_NEW_REDEFINE_NEW 0</font>
<font color="#a020f0">#include </font><font color="#ff00ff">&quot;debug_new.h&quot;</font>
<font color="#a020f0">#else</font>
<font color="#a020f0">#define DEBUG_NEW </font><font color="#804040"><b>new</b></font>
<font color="#a020f0">#endif</font>
</pre>
</blockquote>
<p>and use <code>DEBUG_NEW</code> where memory tracing is needed
(consider global substitution).
</p>
<p>Users might choose to define <code>_DEBUG_NEW_EMULATE_MALLOC</code>,
and <span class="filename">debug_new.h</span> will emulate
<strong>malloc</strong> and <strong>free</strong> with
<strong>debug_new</strong> and <strong>delete</strong>, causing
<strong>malloc</strong> and <strong>free</strong> in a translation unit
including <span class="filename">debug_new.h</span> to be traced. Three
global variables are used to adjust the behaviour of <em>debug_new</em>:
<code>new_output_fp</code>, default to <code>stderr</code>, is the
stream pointer to output information about memory leaks (traditional C
streams are preferred to C++ iostreams since the former is simpler,
smaller, and has a longer and more predictable lifetime);
<code>new_verbose_flag</code>, default to <code>false</code>, will cause
every <code>new</code>/<code>delete</code> to output trace messages when
set to <code>true</code>; <code>new_autocheck_flag</code>, default to
<code>true</code> (which will cause the program to call
<code>check_leaks</code> automatically on exit), will make users have to
call <code>check_leaks</code> manually when set to <code>false</code>.
</p>
<p>One thing to notice is that it might be impossible to ensure that
the destruction of static objects occur before the automatic
<code>check_leaks</code> call, since the call itself is issued from the
destructor of a static object in <span class="filename">debug_new.cpp</span>.
I have used several techniques to better the case. For MSVC, it is quite
straightforword: &ldquo;<code>#pragma init_seg(lib)</code>&rdquo; is
used to adjust the order of object construction/destruction. For other
compilers without such a compiler directive, I use a counter class as
proposed by Bjarne (<a href="#Stroustrup1997">[Stroustrup1997]</a>,
section 21.5.2) and can ensure <code>check_leaks</code> will be
automatically called <em>after</em> the destruction of all objects
defined in translation units that include <span
class="filename">debug_new.h</span>. For static objects defined in C++
libraries instead of the user code, there is a last resort:
<code>new_verbose_flag</code> will be set to <code>true</code> after the
automatic <code>check_leaks</code> call, so that all later
<strong>delete</strong> operations along with number of bytes still
allocated will be printed. Even if there is a misreport on memory
leakage, we can manually confirm that no memory leakage happens if the
later <strong>delete</strong>s finally report that &ldquo;<samp>0 bytes
still allocated</samp>&rdquo;.
</p>
<p><em>Debug_new</em> will report on <strong>delete</strong>ing an
invalid pointer (or a pointer twice), as well as on mismatches of
<strong>new</strong>/<nobr><strong>delete[]</strong></nobr> or
<nobr><strong>new[]</strong></nobr>/<strong>delete</strong>. A
diagnostic message will be printed and the program will
<strong>abort</strong>.
</p>
<p><em>Exception safety</em> and <em>thread safety</em> are worth
their separate sections. Please read on.
</p>

<h2>Exception in the constructor</h2>
<p>Let&rsquo;s look at the following simple program:
</p>
<blockquote>
<pre class="code">
<font color="#a020f0">#include </font><font color="#ff00ff">&lt;stdexcept&gt;</font>
<font color="#a020f0">#include </font><font color="#ff00ff">&lt;stdio.h&gt;</font>

<font color="#2e8b57"><b>void</b></font>* <font color="#804040"><b>operator</b></font> <font color="#804040"><b>new</b></font>(<font color="#2e8b57"><b>size_t</b></font> size, <font color="#2e8b57"><b>int</b></font> line)
{
    printf(<font color="#ff00ff">&quot;Allocate </font><font color="#6a5acd">%u</font><font color="#ff00ff"> bytes on line </font><font color="#6a5acd">%d</font><font color="#6a5acd">\n</font><font color="#ff00ff">&quot;</font>, size, line);
    <font color="#804040"><b>return</b></font> <font color="#804040"><b>operator</b></font> <font color="#804040"><b>new</b></font>(size);
}

<font color="#2e8b57"><b>class</b></font> Obj {
<font color="#804040"><b>public</b></font>:
    Obj(<font color="#2e8b57"><b>int</b></font> n);
<font color="#804040"><b>private</b></font>:
    <font color="#2e8b57"><b>int</b></font> _n;
};

Obj::Obj(<font color="#2e8b57"><b>int</b></font> n) : _n(n)
{
    <font color="#804040"><b>if</b></font> (n == <font color="#ff00ff">0</font>) {
        <font color="#804040"><b>throw</b></font> std::runtime_error(<font color="#ff00ff">&quot;0 not allowed&quot;</font>);
    }
}

<font color="#2e8b57"><b>int</b></font> main()
{
    <font color="#804040"><b>try</b></font> {
        Obj* p = <font color="#804040"><b>new</b></font>(<font color="#ff00ff">__LINE__</font>) Obj(<font color="#ff00ff">0</font>);
        <font color="#804040"><b>delete</b></font> p;
    } <font color="#804040"><b>catch</b></font> (<font color="#2e8b57"><b>const</b></font> std::runtime_error&amp; e) {
        printf(<font color="#ff00ff">&quot;Exception: </font><font color="#6a5acd">%s</font><font color="#6a5acd">\n</font><font color="#ff00ff">&quot;</font>, e.what());
    }
}
</pre>
</blockquote>
<p>Any problems seen? In fact, if we compile it with MSVC, the warning
message already tells us what has happened:
</p>
<blockquote><samp>test.cpp(27) : warning C4291: 'void *__cdecl operator
new(unsigned int,int)' : no matching operator delete found; memory will
not be freed if initialization throws an exception</samp></blockquote>
<p>Try compiling and linking <span class="filename">debug_new.cpp</span>
also. The result is as follows:
</p>
<blockquote>
<pre>
Allocate 4 bytes on line 27
Exception: 0 not allowed
Leaked object at 00341008 (size 4, &lt;Unknown&gt;)
</pre>
</blockquote>
<p>There is a memory leak!
</p>
<p>Of course, this might not be a frequently encountered case. However,
who can ensure that the constructors one uses never throw an exception?
And the solution is not complicated; it just asks for a compiler that
conforms well to the C++ standard and allows the definition of a
placement deallocation function (<a href="#CPP1998">[C++1998]</a>,
section 5.3.4; drafts of the standard might be found on the Web, such as
<a href="http://www.csci.csusb.edu/dick/c++std/cd2/expr.html#expr.new">
here</a>). Of compilers I have tested, GCC (2.95.3 or higher) and MSVC
(6.0 or higher) support this feature quite well, while Borland C++
Compiler 5.5.1 and Digital Mars C++ compiler (all versions up to 8.38)
do not. In the example above, if the compiler supports, we should
declare and implement an &ldquo;<code>operator delete(void*,
int)</code>&rdquo; to recycle the memory allocated by
<code>new(__LINE__)</code>; if the compiler does not, macros need to be
used to make the compiler ignore the relevant declarations and
implementations. To make <em>debug_new</em> compile under such a
non-conformant compiler, users need to define the macro
<code>HAS_PLACEMENT_DELETE</code> (Update:
The macro name is <code>HAVE_PLACEMENT_DELETE</code> from <a
href="/#nvwa">Nvwa</a> version 0.8) to <code>0</code>, and take
care of the exception-in-constructor problem themselves. I wish you did
not have to do this, since in that case your compiler is really out of
date!
</p>

<h2>Thread safety</h2>
<p>My original version of <em>debug_new</em> was not thread-safe. There
were no synchronization primitives in the standard C++ language, and I
was unwilling to rely on a bulky third-party library. At last I decided
to write my own thread-transparency layer, and the current
<em>debug_new</em> relies on it. This layer is thin and simple, and its
interface is as follows:
</p>
<blockquote>
<pre class="code">
<font color="#2e8b57"><b>class</b></font> fast_mutex
{
<font color="#804040"><b>public</b></font>:
    <font color="#2e8b57"><b>void</b></font> lock();
    <font color="#2e8b57"><b>void</b></font> unlock();
};
</pre>
</blockquote>
<p>It supports POSIX threads and Win32 threads currently, as well as a
<em>no-threads</em> mode. Unlike Loki (<a
href="#Alexandrescu2001">[Alexandrescu2001]</a>) and some other
libraries, threading mode is not to be specified in the code, but detected
from the environment. It will automatically switch on multi-threading
when the <nobr><code>-MT</code></nobr>/<code><nobr>-MD</nobr></code>
option of MSVC, the <nobr><code>-mthreads</code></nobr> option of MinGW
GCC, or the <nobr><code>-pthread</code></nobr> option of GCC under POSIX
environments, is used. One advantage of the current implementation is
that the construction and destruction of a static object using a static
<strong>fast_mutex</strong> not yet constructed or already destroyed are
allowed to work (with <code>lock</code>/<code>unlock</code> operations
ignored), and there are re-entry checks for
<code>lock</code>/<code>unlock</code> operations when the preprocessing
symbol <code>_DEBUG</code> is defined.
</p>
<p>Directly calling <code>lock</code>/<code>unlock</code> is
error-prone, and I generally use an RAII (<em>r</em>esource
<em>a</em>cquisition <em>i</em>s <em>i</em>nitialization; <a
href="#Stroustrup1997">[Stroustrup1997]</a>, section 14.4.1) helper
class. The code is short and I list it here in full:
</p>
<blockquote>
<pre class="code">
<font color="#2e8b57"><b>class</b></font> fast_mutex_autolock
{
    fast_mutex&amp; _M_mtx;
<font color="#804040"><b>public</b></font>:
    <font color="#2e8b57"><b>explicit</b></font> fast_mutex_autolock(fast_mutex&amp; __mtx) : _M_mtx(__mtx)
    {
        _M_mtx.lock();
    }
    ~fast_mutex_autolock()
    {
        _M_mtx.unlock();
    }
<font color="#804040"><b>private</b></font>:
    fast_mutex_autolock(<font color="#2e8b57"><b>const</b></font> fast_mutex_autolock&amp;);
    fast_mutex_autolock&amp; <font color="#804040"><b>operator</b></font>=(<font color="#2e8b57"><b>const</b></font> fast_mutex_autolock&amp;);
};
</pre>
</blockquote>
<p>I am quite satisfied with this implementation and its application in
the current <em>debug_new</em>.
</p>

<h2>Special improvement with gcc/binutils</h2>
<p>Using macros has intrinsic problems: it cannot work directly with
placement <strong>new</strong>, for it is not possible to expand an
expression like &ldquo;<code>new(special) MyObj</code>&rdquo; to record
file/line information without prior knowledge of the
&ldquo;<code>special</code>&rdquo; stuff. What is more, the definition
of per-class <strong>operator new</strong> will not work since the
preprocessed code will be like &ldquo;<code>void* operator
new("some_file.cpp", 123)(size_t ...)</code>&rdquo; &mdash; the compiler
will not love this.
</p>
<p>The alternative is to store the instruction address of the caller of
<strong>operator new</strong>, and look up for the source line if a leak
is found. Obviously, there are two things to do:
</p>
<ul>
  <li>Get the caller address of <strong>operator new</strong>;</li>
  <li>Convert the caller address to a source position.</li>
</ul>
<p>There is no portable way to achieve these, but the necessary support
has already been there for ready use if the GNU toolchain is used.
Let&rsquo;s just look at some GNU documentation:
</p>
<blockquote>
<pre>
`__builtin_return_address (LEVEL)'
     This function returns the return address of the current function,
     or of one of its callers.  The LEVEL argument is number of frames
     to scan up the call stack.  A value of `0' yields the return
     address of the current function, a value of `1' yields the return
     address of the caller of the current function, and so forth.

     The LEVEL argument must be a constant integer.

     On some machines it may be impossible to determine the return
     address of any function other than the current one; in such cases,
     or when the top of the stack has been reached, this function will
     return `0'.
</pre>
<div align="right">(<em>gcc</em> info page)</div>
</blockquote>
<blockquote>
<pre>
addr2line
*********

     addr2line [ -b BFDNAME | --target=BFDNAME ]
               [ -C | --demangle[=STYLE ]
               [ -e FILENAME | --exe=FILENAME ]
               [ -f | --functions ] [ -s | --basename ]
               [ -H | --help ] [ -V | --version ]
               [ addr addr ... ]

   `addr2line' translates program addresses into file names and line
numbers.  Given an address and an executable, it uses the debugging
information in the executable to figure out which file name and line
number are associated with a given address.

   The executable to use is specified with the `-e' option.  The
default is the file `a.out'.
</pre>
<div align="right">(<em>binutils</em> info page)</div>
</blockquote>
<p>So the implementation is quite straightforward and like this:
</p>
<blockquote>
<pre class="code">
<font color="#2e8b57"><b>void</b></font>* <font color="#804040"><b>operator</b></font> <font color="#804040"><b>new</b></font>(<font color="#2e8b57"><b>size_t</b></font> size) <font color="#804040"><b>throw</b></font>(std::bad_alloc)
{
    <font color="#804040"><b>return</b></font> <font color="#804040"><b>operator</b></font> <font color="#804040"><b>new</b></font>(size, __builtin_return_address(<font color="#ff00ff">0</font>), <font color="#ff00ff">0</font>);
}
</pre>
</blockquote>
<p>When a leak is found, <em>debug_new</em> will try to convert the
stored caller address to the source position by
<strong>popen</strong>ing an <strong>addr2line</strong> process, and
display it if something useful is returned (it should be the case if
debugging symbols are present); otherwise the stored address is
displayed. One thing to notice is that one must tell <em>debug_new</em>
the path/name of the process to make <strong>addr2line</strong>
work. I have outlined the ways in the <a
href="http://nvwa.sourceforge.net/doc/0.8/debug__new_8cpp.html#61bfe5a99f6c19a53f71c1d01bdfd982">doxygen
documentation</a>.
</p>
<p>If you have your own routines to get and display the caller address,
it is also easy to make <em>debug_new</em> work with it. You may check
the <a href="http://nvwa.cvs.sourceforge.net/*checkout*/nvwa/nvwa/debug_new.cpp">source
code</a> for details. Look for <code>_DEBUG_NEW_CALLER_ADDRESS</code>
and <code>print_position_from_addr</code>.
</p>

<a name="update2007"></a>
<h2>Important update in 2007</h2>
<p>With an idea coming from <a
href="http://groups.google.com/group/comp.lang.c++.moderated/browse_thread/thread/7089382e3bc1c489/85f9107a1dc79ee9?#85f9107a1dc79ee9">Greg
Herlihy&rsquo;s post</a> in comp.lang.c++.moderated, a better solution
is implemented. Instead of defining <strong>new</strong> to
&ldquo;<code>new(__FILE__, __LINE__)</code>&rdquo;, it is now defined to
&ldquo;<code>__debug_new_recorder(__FILE__, __LINE__) -&gt;*
new</code>&rdquo;. The most significant result is that placement
<strong>new</strong> can be used with <em>debug_new</em> now!  Full
support for <strong>new(std::nothrow)</strong> is provided, with its
null-returning error semantics (by <a
href="http://nvwa.sourceforge.net/doc/0.8/debug__new_8cpp.html#3345b568583c37212e07e475097ea4fb">default</a>).
Other forms (like &ldquo;<code>new(buffer) Obj</code>&rdquo;) will
probably result in a run-time warning, but not compile-time or run-time
errors &mdash; in order to achieve that, magic number signatures are
added to detect memory corruption in the free store. Memory corruption
will be checked on freeing the pointers and checking the leaks, and a
new function <code>check_mem_corruption</code> is added for your
on-demand use in debugging. You may also want to define <a
href="http://nvwa.sourceforge.net/doc/0.8/debug__new_8cpp.html#46dc09d4f6f36d94ad0df7df95871009"><code>_DEBUG_NEW_TAILCHECK</code></a>
to something like <code>4</code> for past-end memory corruption check,
which is off by default to ensure performance is not affected.
</p>
<p>The code was heavily refactored during the modifications. I was quite
satisfied with the new code, and I released <a
href="http://sourceforge.net/project/showfiles.php?group_id=104822&amp;package_id=112773&amp;release_id=565022">Nvwa
0.8</a> as a result.
</p>

<h2>Summary</h2>
<p>So I have presented my small memory leakage detector. I&rsquo;ll make
a summary here, and you can also consult the online <a
href="http://nvwa.sourceforge.net/doc/current/debug__new_8cpp.html">doxygen
documentation</a> for the respective descriptions of the functions,
variables, and macros.
</p>
<p>This implementation is relatively simple. It is lacking in features
when compared with commercial applications, like Rational Purify, or
even some open-source libraries. However, it is
</p>
<ul>
  <li><em>Cross-platform</em> and <em>portable</em>: Apart from the
code handling threading (which is separated from the main code) and
providing special GCC support (which is automatically on when GCC is
detected), only standard language features are used. It should compile
under modern C++ compilers. It is known to work with GCC (2.95.3 and
later), MSVC 6/7.1, and Borland C++ Compiler 5.5.1.</li>
  <li><em>Easy to use</em>: Because &ldquo;<code>void* operator
new(size_t)</code>&rdquo; is overloaded too, memory leaks could be
detected without including my header file. &mdash; I myself use it this
way habitually in nearly every C++ program. &mdash; Generally, I check
for the leak position only after I see memory leaks reported by
<em>debug_new</em>.</li>
  <li><em>Flexible</em>: Its behaviour can be tailored by macros at
compile time.</li>
  <li><em>Efficient</em>: It has a very low overhead, and it can be
used in debugging applications that require high performance.</li>
  <li><em>Open-source</em>: It is released in the <a
href="http://www.opensource.org/licenses/zlib-license.php">zlib/libpng
licence</a> and you have the freedom to use, change, or redistribute as
you like.</li>
</ul>
<p>With the recent improvements, some of the old restrictions are gone.
The macro <code>new</code> or <code>DEBUG_NEW</code> in <span
class="filename">debug_new.h</span> can mostly work if the
<strong>new</strong>ed object has <strong>operator new</strong>s as
class member functions, or if <strong>new(std::nothrow)</strong> is used
in the code, though the macro <code>new</code> must be turned off when
defining any <strong>operator new</strong>s. Even in the worst case,
linking only <span class="filename">debug_new.cpp</span> should always
work, as long as the allocation operation finally goes to the global
<strong>operator new(size_t)</strong> or <strong>operator new(size_t,
std::nothrow_t)</strong>.
</p>
<p>Source is available, for your programming pleasure, in the <a
href="http://nvwa.cvs.sourceforge.net/nvwa/nvwa/">CVS</a> (most up to
date) or <a
href="http://sourceforge.net/project/showfiles.php?group_id=104822">download</a>
of <a href="http://sourceforge.net/projects/nvwa/">Stones of Nvwa</a>.
</p>
<p>May the Source be with you!
</p>

<h2>Bibliography</h2>
<table width="100%" cellpadding="2" cellspacing="2" border="0">
  <tbody>
    <tr>
      <td valign="top"><a name="Alexandrescu2001"></a>[Alexandrescu2001]&nbsp;</td>
      <td valign="top">Andrei Alexandrescu. <em>Modern C++ Design:
      Generic Programming and Design Patterns Applied</em>.
      Addison-Wesley, 2001.</td>
    </tr>
    <tr>
      <td valign="top"><a name="CPP1998"></a>[C++1998]&nbsp;</td>
      <td valign="top">ISO/IEC. <em>Programming
      Languages &mdash; C++</em>. Reference Number ISO/IEC 14882:1998(E),
      1998.</td>
    </tr>
    <tr>
      <td valign="top"><a name="Stroustrup1997"></a>[Stroustrup1997]&nbsp;</td>
      <td valign="top">Bjarne Stroustrup. <em>The C++ Programming
      Language</em> (Third Edition). Addison-Wesley, 1997.</td>
    </tr>
  </tbody>
</table>

<hr>
<p>HTML for code syntax highlighting is generated by <a
href="http://www.vim.org/">Vim</a>
</p>
<p><small>
<strong>Note:</strong> Although I would <em>love</em> to, I did not
succeed in making this page conform to the HTML 4.01 specification. I
guess W3C is to blame. Why should <code>&lt;font color=...&gt;</code>
be forbidden inside a <code>&lt;pre&gt;</code> block (Vim currently
generate HTML code this way), while the clumsier <code>&lt;span
style=&quot;color: ..."&gt;</code> is allowed? However, this is fixable
after all (I have already written a converter indeed). There is worse to
come: <code>&lt;nobr&gt;</code> is not a valid tag. When a major browser
could break a line after a <nobr>&ldquo;<code>-</code>&rdquo;</nobr> and
no mechanisms are provided by the standard to achieve the no-<wbr>breaking
effect, using something like <code>&lt;nobr&gt;</code> is inevitable
(and I can name other cases where <code>&lt;nobr&gt;</code> is needed).
I cannot fix the browser, so I have to choose to break the
standard. Detailed online information about this problem can be found <a
href="http://www.cs.tut.fi/~jkorpela/html/nobr.html">here</a>.
</small></p>
<p>2004-3, Chinese version first published <a
href="http://www-900.ibm.com/developerWorks/cn/linux/l-mleak2/index.shtml">here</a>
at IBM developerWorks China
<br>
2004-11-28, rewritten in English (at last) by Wu Yongwei
<br>
2007-12-31, last updated by Wu Yongwei
</p>
<center>
<p><a rel="license" href="http://creativecommons.org/licenses/by-sa/2.5/" class="noprintlink"><img style="border-width:0" src="http://i.creativecommons.org/l/by-sa/2.5/88x31.png"/></a><br/>This <span xmlns:dc="http://purl.org/dc/elements/1.1/" href="http://purl.org/dc/dcmitype/Text" rel="dc:type">work</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/2.5/">Creative Commons Attribution-Share Alike 2.5 Licence</a>.</p>
<p><a href="/">Return to Main</a></p>
</center>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-4153155-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
</html>
