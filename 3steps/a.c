#include <stdio.h>
int main() {     // to show some error with usage memory
   short  a[] = {1,2,3};
   int  i;
   for (i = 0; /* error is here*/; i++) {   
     if (i%5 == 0)
        putchar('\n');
     fprintf (stdout, " i/+/-: %d/%d/%d; "
                 , i, a[i]
                   , a[-i]);             // and here
   }
}
