#include <stdio.h>
int main() {
   int x = 1;
   int b = 1, c;
   // example 1
   c = b + b++;          //  b++ ����������� ����� ���������, ��������� 3
   printf( "\n %d", c);  // 3
   // example 2
   x += x++ + ++x;    //  result depends on the compiler
   //         1+1       g++
   //   2+1
   //      3+2
   // 2 + 5          -> 7
   printf("\n%d\n", x);
}
