#include <iostream>
#include <fstream>
#include <windows.h>

const int BUFSIZE = 1000;

int main(int argc, char* argv[]) {
  FILE* out = fopen("out.txt", "wb");
  if (out) {
        char     buf1 [BUFSIZE+1];
        wchar_t  buf2 [BUFSIZE+1];
      
        while (fgets(buf1, BUFSIZE, stdin) != NULL) {		
        //			WideCharToMultiByte(CP_ACP, 0, buf1, BUFSIZE, buf2, BUFSIZE);
          	MultiByteToWideChar(1251, 0, buf1, BUFSIZE, buf2, BUFSIZE);
          	fputws(buf2, out);
      }
      fclose (out);
  }
}
