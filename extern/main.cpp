#include <stdio.h>
#include <stdlib.h>
extern  double sum    (double l, double r);
extern  double divide (double l, double r);

int verbose = 1;      // global variable

int main(int argc, char * argv[]) {
  if (argc >= 4 ) {
     double r = 0.0;

  if (verbose)
     fprintf(stderr, "\ni've got: %s %s %s", argv[1], argv[2], argv[3]);

     if (*argv[2] == '+' )
        r =  sum (atof(argv[1]), atof(argv[3]));
      else if (*argv[2] == '/' )
        r =  divide(atof(argv[1]), atof(argv[3]));
      else 
        goto end;   // it is some provocation
      printf ("%e", r);
      return 0;
  }
  end:
  printf ("nothing to do");
  return 1;
}
