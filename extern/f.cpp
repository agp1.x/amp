#include <stdio.h>
extern int verbose;
static int foo = 1; // static  => it is impossible to see 
                    // variable foo outside of current file
double sum  (double l, double r) { 
  if (verbose)
     fprintf(stderr, "\nsum is here: %e/%e", l, r);
  return l + r;
}
double divide (double l, double r) {
  if (verbose)
     fprintf(stderr, "\ndivide is here: %e/%e", l, r);
  return l / r;
}

