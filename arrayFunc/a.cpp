#include <stdio.h>
#include "intArray"

#define  ASZ (1)
#define  BSZ (9)

int main() {     // to show some error with usage memory
   int  a[ASZ] = {0};
   int  b[BSZ] = {0};
   ini(a, ASZ);
   ini(b, BSZ);
   printf("array is unsorted");
   print(a, ASZ);
   sort (a, ASZ);
   printf("\narray is sorted");
   print(a, ASZ, 5);
}
