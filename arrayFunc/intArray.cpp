#include <stdio.h>
#include <stdlib.h>
#include "intArray"


#include "ini.c1"   // �� ���� ��� ������ � �������� ��������
#include "print.c1" // ��� ��� �������� ��� ����, ����� � ��������
                    // � �������� ����� ���� �������� ��������� �������

  void sort (int a[], int n) {
    int tmp;
    int mI;  // ������ �������� ���������
    int cI;  // ������ ����������� ����� ��� ���.���������
    for (; n>1 ; n--) {
      cI = n-1;
      mI =  maxIndex(a, n);
      if (mI < cI)
         SWAP2(a[mI], a[cI], tmp);
    }
    return;
  }

  int  maxIndex  (int a[], int aSz) {
    int m    = a[0];
    int mInd = 0;
    int i;
    for (i = 1;          // ������� �������
         i < aSz;        // �������� ��� ����?
             i++         // ������� ��������
                ) {   
      if ( m < a[i]) {   // ����������� ������� ������ ��������?
         m    = a[i];    //  ��������� ��������
         mInd = i;       //  � ��� ������
      }
    }
    return  mInd;
  }
