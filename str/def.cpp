#include <stdio.h>
int main() {     
  char a1[20]; 
  char a2[20] ={'a', 'b', 'c', 'd'}; 
  char a3[20] ="hello";  
  char a4[]   ="bye";  
  char *ptr   = (char*)"no";
  printf ("\n array2: '%s'\n array3: '%s'\n array4: '%s'", a2, a3, a4);
  a4[0] = 'y';
  a4[1] = 'e';
  a4[2] = 's';
  printf ("\n new array4: '%s'",  a4);
  *a4     = 'b';
  *(a4+1) = 'y';
  *(a4+2) = 'e';
  printf ("\n old array4: '%s'",  a4);
  *(ptr+1) = 0;      // error is here
}