  #include <stdio.h>

  void print(    int  a[], int aSz, int eNums){
    int i;
    for (i = 0; i < aSz; i++) {   
      if (i%eNums == 0)
         printf ("\n");
      printf ( " i/elem: %3d/%d; "
                                   , i, a[i]);
    }
    return;
  }
