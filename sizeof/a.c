#include <stdio.h>
int main() {
    int    a;
    double b;
    short  x[] = {1,2,3,4};
    fprintf (stdout, 
     "length of int/a: %d/%d, double/b: %d/%d, short/x/array: %d/%d/%d"
                      , sizeof(int), sizeof(a)
                                        , sizeof(double), sizeof(b)
                                               , sizeof(short), sizeof(x)
                                                 , sizeof (x)/sizeof(short));
}
