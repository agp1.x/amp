#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
int main() {
  int x = atoi("111111111111");  // number is to big, it's error
  printf(  "error: %d: %s", errno,   strerror(errno) );
  char buffer[100];
  double source = 3.1415926535;
  itoa( 123,  buffer, 10);
  printf( "\nitoa (%d) -> '%s'",   123, buffer);

  gcvt( source, 8, buffer); 
  printf( "\ngcvt ( %f) ->  '%s'",  source, buffer);

//   printf ("\nstrcasecmp %d", strcasecmp("aa", "aa"));  // g++
//   printf ("\nstricmp %d", stricmp("aa", "aa"));        // Microsoft
}