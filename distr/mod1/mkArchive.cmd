@echo off

SET NAMEZIP=N0

del  .*  *.exe  *.obj  *.tmp /s

if exist local.cmd call local.cmd

if %NAMEZIP%==N0  SET NAMEZIP=%1
if %NAMEZIP%==%1  shift
cd ..

G:\bin\7-Zip\7z.exe a -r %NAMEZIP%.%1.zip %NAMEZIP%  -x!local.cmd -x!mkArchive.cmd 