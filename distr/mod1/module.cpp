﻿#include <iostream>
using namespace std;
int arraySize, mode;
int main(int argc, char* argv[])
{
	if (argc > 2)
	{
		cout << "DIRECTION: " << argv[1] << " SIZE: " << argv[2] << endl;
		arraySize = atoi(argv[2]);
		mode = atoi(argv[1]);

		char** Mas = new char* [arraySize];
		for (int i = 0; i < arraySize; i++)
			Mas[i] = new char[arraySize];

		for (char i = 0; i < arraySize; i++) {
			for (char j = 0; j < arraySize; j++) {
				Mas[i][j] = ' ';
				switch (mode)
				{
				case 1:
					if (i > j)
						Mas[i][j] = '*';
					break;
				case 2:
					if (i < arraySize - j)
						Mas[i][j] = '*';
					break;
				case 3:
					if (i < j)
						Mas[i][j] = '*';
					break;
				case 4:
					if (i > arraySize - j)
						Mas[i][j] = '*';
					break;
				default:
					break;
				}
			}
		}
		for (char i = 0; i < arraySize; i++) {
			for (char j = 0; j < arraySize; j++) {
				cout << Mas[i][j] << " ";
			}
			cout << endl;
		}
	}
	else
	{
		cout << "Enter direction and size please !!!" << endl;
	}
}
